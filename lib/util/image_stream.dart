import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lab3_bai2/model/image.dart';

class MyImageStream {
  Stream<ImageModel> getImage() async* {
    List<ImageModel> images = [];

    Future _fetchImages() async {
      final response = await http.get(Uri.parse(
          'https://jsonplaceholder.typicode.com/photos?_start=0&_limit=100'));

      if (response.statusCode == 200) {
        List<dynamic> list = json.decode(response.body);

        List<ImageModel> listParsed =
        list.map((tagJson) => ImageModel.fromJson(tagJson)).toList();

        images = listParsed;
      } else {
        throw Exception('Failed to load album');
      }
    }

    _fetchImages();

    yield* Stream.periodic(const Duration(seconds: 5), (int t) {
      return images[t % images.length];
    });
  }
}
