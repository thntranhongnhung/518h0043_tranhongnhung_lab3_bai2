import 'package:flutter/material.dart';
import 'package:lab3_bai2/model/image.dart';
import '../util/image_stream.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  MyImageStream imageStream = MyImageStream();

  List<ImageModel> listImages = [];

  startStream() async {
    imageStream.getImage().listen((image) {
      setState(() {
        listImages.add(image);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    startStream();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Row(
            children: const [
              Text("Images",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1.0,
                  crossAxisSpacing: 10.0,
                  mainAxisSpacing: 10.0),
              itemCount: listImages.length,
              itemBuilder: (context, index) {
                return ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(listImages[index].url));
              },
            ),
          ),
        ],
      ),
    );
  }
}
